package org.example;

public interface ICoffee {
    boolean cook();
    boolean pour();
    boolean serve();

}
