package org.example;

import java.util.Random;

public class Americano implements ICoffee{
    Random random = new Random();

    @Override
    public boolean cook() { ;
        System.out.println("cooking americano");
        return random.nextInt(100) < 95;
    }

    @Override
    public boolean pour() {
        System.out.println("pouring americano");
        return random.nextInt(100) < 95;
    }

    @Override
    public boolean serve() {
        System.out.println("serving americano");
        return random.nextInt(100) < 95;
    }
}
