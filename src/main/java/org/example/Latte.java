package org.example;

import java.util.Random;
import java.util.Timer;

public class Latte implements ICoffee {
    Random random = new Random();

    @Override
    public boolean cook() {
        //int rnd = (int) (random.nextInt(100)); //int x = random.nextInt(max - min + 1) + min;
        //boolean r = rnd < 95;
        System.out.println("cooking latte");
        return random.nextInt(100) < 95;
    }

    @Override
    public boolean pour() {
        System.out.println("pouring latte");
        return random.nextInt(100) < 95;
    }

    @Override
    public boolean serve() {
        System.out.println("serving latte");
        return random.nextInt(100) < 95;
    }
}
