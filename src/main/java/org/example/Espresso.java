package org.example;

import java.util.Random;

public class Espresso implements ICoffee {
    Random random = new Random();

    @Override
    public boolean cook() {
        ;
        System.out.println("cooking espresso");
        return random.nextInt(100) < 95;
    }

    @Override
    public boolean pour() {
        System.out.println("pouring espresso");
        return random.nextInt(100) < 95;
    }

    @Override
    public boolean serve() {
        System.out.println("serving espresso");
        return random.nextInt(100) < 95;
    }
}
