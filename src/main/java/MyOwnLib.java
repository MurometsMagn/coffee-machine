import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * If some method is often used in dif projects.
 * Smolentsev Il'ya
 */

public class MyOwnLib {
    private static Scanner scanner = new Scanner(System.in);

    public static int keyboardInputInt(int minValue, int maxValue) {
        int inputData;
        while (true) {
            try {
                inputData = scanner.nextInt();
                if (inputData >= minValue && inputData <= maxValue)
                    break;
                else
                    System.out.println("некоректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        }
        return inputData;
    }

    public static String keyboardInputString(String requestToUser) {
        String inputData; //responseFromUser
        do {
            System.out.println(requestToUser);
            inputData = scanner.nextLine();
        } while (inputData.trim().isEmpty());
        return inputData;
    }
}
