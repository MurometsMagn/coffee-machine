package org.example;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Кофейный аппарат может готовить несколько видов кофе: Американо, Латте и Эспрессо.
 * Пользователь выбирает тип кофе, кофе приготавливается, наливается и выдается пользователю
 * kebab-case
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int order = 0;

        System.out.println("choose coffee and press it's number: ");
        System.out.println("1 - americano ");
        System.out.println("2 - espresso ");
        System.out.println("3 - latte ");


        while (true) {
            try {
                order = scanner.nextInt();
                if (order >= 1 && order <= 3) {
                    break;
                } else
                    System.out.println("некоректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        }

        CoffeeMachine coffeeMachine = new CoffeeMachine();
        coffeeMachine.setCoffeeType(CoffeeMachine.SelectType.values()[order - 1]);

        if (!coffeeMachine.cook()) {
            System.out.println("can't cook coffee, sorry, try tomorrow again");
            return;
        }
        if (!coffeeMachine.pour()){
            System.out.println("can't pour coffee, wait for a while");
            return;
        }
        if (!coffeeMachine.serve()) {
            System.out.println("can't serve coffee, pay once more))");
        }
    }
}
