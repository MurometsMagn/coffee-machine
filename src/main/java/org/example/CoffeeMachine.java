package org.example;

public class CoffeeMachine implements ICoffee{
    private ICoffee coffee = new Americano(); //начальное значение

    @Override
    public boolean cook() {
        return coffee.cook();
    }

    @Override
    public boolean pour() {
        return coffee.pour();
    }

    @Override
    public boolean serve() {
        return coffee.serve();
    }

    enum SelectType {
        AMERICANO {
            @Override
            public ICoffee createCoffee() {
                return new Americano();
            }
        },
        ESPRESSO {
            @Override
            public ICoffee createCoffee() {
                return new Espresso();
            }
        },
        LATTE {
            @Override
            public ICoffee createCoffee() {
                return new Latte();
            }
        };

        public abstract ICoffee createCoffee();
    }

    public void setCoffeeType(SelectType selectType) {
        coffee = selectType.createCoffee();
    }
}